package main

import (
	"fmt"
	"log"
	"time"

	"github.com/cheggaaa/pb/v3"
	"github.com/sajari/regression"
)

func trainModel(x, y []float64, model *regression.Regression, progressCh chan<- struct{}) {
	for i, xVal := range x {
		model.Train(regression.DataPoint(y[i], []float64{xVal}))
		progressCh <- struct{}{}
		time.Sleep(100 * time.Millisecond) // Simulate some computation time
	}
	close(progressCh)
}

func main() {
	// Sample data
	x := []float64{1, 2, 3, 4}
	y := []float64{2, 4, 6, 8}

	// Create a new progress bar with the total number of data points
	bar := pb.StartNew(len(x))

	// Create a new regression model
	model := new(regression.Regression)
	model.SetObserved("Y")
	model.SetVar(0, "X")

	// Create a channel to receive progress updates
	progressCh := make(chan struct{})

	// Start the training process in a separate goroutine
	go trainModel(x, y, model, progressCh)

	// Continuously update the progress bar until the channel is closed
	for range progressCh {
		bar.Increment()
	}

	// Finish the progress bar
	bar.Finish()

	// Train the model
	if err := model.Run(); err != nil {
		log.Fatal(err)
	}

	// Make a prediction for a new data point
	newX := 5.0
	prediction, err := model.Predict([]float64{newX})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Prediction for x = 5: %.2f\n", prediction)
}
